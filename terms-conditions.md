---
title: "Terms & Conditions"
permalink: /terms-conditions/
referred-as: MARP
layout: boxdrawn
no_sidebar: true
---
These are the terms and conditions to use this website, this website is owned and operated by Miguel Ángel Rodríguez, who may be referred as "{{page.referred-as}}" from now on, or may be referred as a plural with the pronouns "we", “us” or "our”, depending on the context.
 
## Aceptance of use of the terms & conditions
 
Your access and use of this website is subject exclusively to these Terms and Conditions. You will not use the Website for any purpose that is unlawful or prohibited by these Terms and Conditions. By using the Website you are fully accepting the terms, conditions and disclaimers contained in this notice. If you do not accept these Terms and Conditions you must immediately stop using the Website.
 
## Use License
 
You may copy, redistribute or make a derivative work of the content in this website (information or images embedded with the information that belongs to us, not software) freely for non-profit purpose under the conditions of the license "[Creative Commons Attribution-NonCommercial 3.0 Unported](https://creativecommons.org/licenses/by-nc/3.0/)", the attribution must specify the name of the author, in case that the content is an article, and a link to the copied content, if the attribution it is on a html document, you must label the link of the content with the following word; “{{page.referred-as}}”. Permission is granted to temporarily download one copy of our software in this website for personal, non-commercial transitory viewing only, any third-party material (files such as images or videos) may be downloaded or viewed according to the licences of each material, you may expect an attribution with a link to the original content to identify each of these third-party materials, any material that has not an attribution, may be treated and used as one of our materials, using that must match these terms & conditions. This is the grant of a license, not a transfer of title, and under this license you may not...

- modify or copy the content without a proper attribution
- use our materials for any commercial purpose
- attempt to decompile or reverse engineer any software contained in this website
- remove any copyright or other proprietary notations from the materials

This license shall automatically terminate if you violate any of these restrictions and may be terminated by us at any time. Upon the termination of this license, you must destroy any downloaded materials on any format.
 
## Disclaimer
 
The materials in this website are provided "AS IS". we make no warranties, expressed or implied, and hereby disclaims and negates all other warranties, including without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights. Further, we do not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on our Internet website or otherwise relating to such materials or on any sites linked to this site.
Limitations
 
In no event shall we or our suppliers be liable for any damages (including, without limitation, damages for loss of data or profit, or due to business interruption) arising out of the use or inability to use the materials in our website, even if we or one of our authorized representative has been notified orally or in writing of the possibility of such damage. Because some jurisdictions do not allow limitations on implied warranties, or limitations of liability for consequential or incidental damages, these limitations may not apply to you.
 
## Revisions and Errata
 
The materials appearing on our website could include technical, typographical, or photographic errors. we do not warrant that any of the materials on our website are accurate, complete, or current. We may make changes to the materials contained on our website at any time without notice. we do not, however, make any commitment to update the materials.
 
## Links
 
we have not reviewed all of the sites linked to our Internet website and we are not responsible for the content of any such linked site. The inclusion of any link does not imply endorsement by us. the Using of any such linked website is at the user's own risk.
 
## Site Terms of Use Modifications
 
We may revise these terms & conditions of use for our website at any time without notice. By using this website you agree with the current version of these Terms and Conditions of Use.


#!/usr/bin/env ruby
require "yaml"

curdir = "#{Dir.getwd}/_posts/legacy-blog/"
tags = []
if Dir.exist?(curdir)
  Dir.each_child(curdir) do |filename|
    filepath = "#{curdir}#{filename}"
    if File.exist?(filepath)
      yaml = YAML.load_file(filepath)
      if !yaml["tags"].nil?
        yaml["tags"].each do |tag|
          tags.push(tag) if !tags.include?(tag)
        end
      end
    end
  end
end

curdir = "#{Dir.getwd}/_tags/"
if Dir.exist?(curdir)
  tags.each do |tag|
    processed_tag = tag.downcase.gsub("+", "-plus").gsub(/\s/, '-')
    tagpath = "#{curdir}#{processed_tag}.md"
    if !File.exists?(tagpath)
      ftag = File.new(tagpath,  "w+")
      ftag.write <<-TAG
---
name: #{processed_tag}
title: #{tag}
layout: emacs-frames
---
TAG
      ftag.close()
    end
  end
end

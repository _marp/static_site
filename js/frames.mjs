let totalHeight = 0

const scroller = document.querySelector(".main.frame .content"), //element that scrolls
      bar = document.querySelector(".main.frame .bar"), //text that will change when scrolling
      minibuffer = document.querySelector(".minibuffer"),
      title = document.head.querySelector("[name~=title][content]").content

function calculateHeight(){
	for( let el of scroller.children ){ totalHeight += el.offsetHeight }
}

function printBuffer(){
    let percent,
		minibufferContent = "",
		scrolledNow = scroller.scrollTop + scroller.offsetHeight

	percent = Math.ceil(( scrolledNow / totalHeight ) * 100) + "%"

    if(scroller.scrollTop == 0){
    	percent = 'Top'
		minibufferContent = "Beginning of buffer"
    }
	else if( scrolledNow == totalHeight || percent > 99 ){
    	percent = 'Bot'
		minibufferContent = "End of buffer"
    }

    minibuffer.innerHTML = minibufferContent
    bar.innerHTML = `&nbsp;U:%%-&nbsp;&nbsp;&nbsp;${title}&nbsp;&nbsp;&nbsp;${percent}&nbsp;&nbsp;&nbsp;(Fundamental)`
}

function init(){

	if(totalHeight > scroller.offsetHeight){
		scroller.addEventListener('scroll', printBuffer)
	}
	bar.innerHTML = `&nbsp;U:%%-&nbsp;&nbsp;&nbsp;${title}&nbsp;&nbsp;&nbsp;Top&nbsp;&nbsp;&nbsp;(Fundamental)`
	document.querySelector(".side.frame .bar").innerHTML = `&nbsp;&nbsp;&nbsp;:---&nbsp;&nbsp;&nbsp;logo.jpg&nbsp;&nbsp;&nbsp;All&nbsp;&nbsp;&nbsp;(Image[imagemagick])`
	document.querySelector(".logo.frame .bar").innerHTML = `&nbsp;U:%%-&nbsp;&nbsp;&nbsp;Sidebar&nbsp;&nbsp;&nbsp;Top&nbsp;&nbsp;&nbsp;(Fundamental)`

}

window.addEventListener('load', init)

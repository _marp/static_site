---
slug: my-top-5-timewasters-errors
title: My top 5 timewasters errors
date: 2013-04-05
draft: true
image_path: /files/default-image.jpg
image_width: 480
image_height: 300
---
I always try to analyze all the mistakes I made in order to improve my performance, you would be amazed how much time can someone waste with some errors that are not that complicated, here are my top 5 timewaster errors.


comparison errors

incredibly, I have wasted several time in this kind of errors, as you may know the assignment operator returns the value that is being assigned, you also may know that the "if" sentence only evaluates if the value is something that can be interpret as a boolean "false" (false, 0, null), so if you miss a simbol in a comparison within an "if" sentence without noticing it, you may find yourself figuring out where your code is failing.
var = "";

solution:
interestingly, I found a long time ago a good practice that comes from a coding convention which encourage to set the comparision with the value to compare first (in case the comparison is between a variable and a single value, not between variables), if you miss a simbol you will get a visible error because you can not assign a value to a simple value (a string or a number).

"" == var; //this is OK

"" = var; //this would throw an error

"copy/paste" errors
Sometimes, in order to expedite the development people usually copy code which have the exact functionality that was required, this usually happens when the team is involved in more than one project (actually, this should not happen within a single project if DRY means something to you), it may also happen when someone gets someone else code from a blog or similar, if you do not pay especial atention to the things that you are doing you will find yourself figuring out why you are getting so many errors.

$("vaina", function(){

     //this was copied
     $.ajax(function(){
     ...
     this.tal.value
     ...
     });
     //and will not work

});

solution:
In these cases is good tp add a comment to the blocks of code that were copied, this is a practice that it should be applied to all the code writen, indicate what the code does and who made it, regarding the copied code you may also add the source and other information that may be interesting or important, these comments shoud be added even if you have modified the code.

"UTF8 BOM" errors (PHP)
Regarding specifically to PHP, there is a dumb error when it happens for the first time you may waste a lot of time trying to solve it, basically PHP is an interpreted programming languages optimized for the web, PHP uses a couple of text files indicated by the webserver in order to operate, each time a PHP code is executed all the HTTP headers, short pieces of information about the message itself, are sent first then the content is sent, PHP allows to send HTTP headers anytime as long as you send those headers BERFORE THE CONTENT, so what is the deal with this "UTF8 BOM", these text files that PHP interprets are normal files as anyone else with various formats as any file, however, if some of those files has a "UTF8 with BOM" format the content of those files will be printed with an additional text

although it is OK, it will not work.
<¿php
session_start();
...

solution:
if you get this kind of error the first thing to verify is the format of the file you are editing, if it says "UTF8 with BOM" just use anything else, if it does not work then you should start seeking for the content that is printed before the header giving problems and either put the header before that piece of content or remove that content.

ramaining debugging lines errors
anyone can desperate at least once even on situations such as debugging, as you may know there are many debugging actions/techniques such as disabling blocks of code until the error disappears or changing specific lines (variable assigments, "if" sentences, ...), a clear evidence of desperation is doing this as fast as possible in any file if necessary, well if you do not leave at least a comment indicating these changes ironically you may generate even more errors instead of solving them.

solution:

Make sure you write the proper comments, in these cases it is something necessary, if you do not do it, you are more likely to make even more problems, you should change and comment everything in a way easy to revert, let us see some examples.

in case of assignments please never delete the real value to assign, instead of that write a comment in a way to back up the change easily;

var = assignment;

var = 30//*/assignment; //DEBUG

in case of conditional sentences may happen the same;

if(var < anothervar){
//code
}

if(true){//*/var < anothervar){ //DEBUG
//code
}

in case of blocks of code;

int var;

for(var=0;var<10;var++){

//code

}

//possibly more code

/* comments about the block
...
int var;

for(var=0;var<10;var++){

//code

}

//possibly more code

...

//*/ ... replacement code ...  //DEBUG

Server time configuration issues

The server time of Venezuela was changed from -4:00 UTC to -4:30 UTC, if you are not aware of this, you may spend houndreds of time figuring out why the AWS does not respond to any request.

EXTRAS:

Use a debugger
php: xDebug & PDT




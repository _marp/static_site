---
slug: using-css-classes-themecustomize-drupal-without-coding
title: Using CSS classes to theme/customize Drupal without coding
summary: Have I told you there are some Drupal modules which can change styles visually through CSS this without coding?
date: 2012-12-28
imgpath: /files/CSS%20Carved%20pumpkin.jpg
imgdescription: "[CSS Carved pumpkin](http://www.flickr.com/photos/mauriz/4059476052/)&nbsp;- [Maurice Svay](http://svay.com/)"
imgwidth: 480
imgheight: 300
tags: [css, theming, drupal-theming, web-design]
---
Have I told you there are some Drupal modules which can change styles visually through CSS this without coding?. Is really important to specify styles with CSS classes in order to reuse the code. Have I told you there are a lot of Drupal modules which have the capability to set CSS classes?. As you may know, CSS is used to define the visual styles in a webpage. CSS can assign those styles whether to specific elements using IDs or Class names for groups of elements. We can use CSS Classes for reusability matter (actually there are a lot of concepts and principles which encourage to keep things reusable and avoid redundancy such as DRY [Don't Repeat Yourself]). If is there a style which can be implemented somewhere else you should define that style with a Class name. Doing this you may implement these styles easier although you may think that you still have to code the changes. That is only theoretically.
**There are phases in the Drupal theming process that do not require the intervention of any IT professional**. **Modules such as Panels and Views let the user create layouts visually. Some of these modules let assign CSS classes visually as well**, so the user may change the content, the layout and all the styles within it. For example, by the time I wrote this article. **The theme that I made had a couple of CSS Classes which can be used to highlight pieces of text, so if I ever want to highlight a pieces of text with my editor (CKeditor). I just have to add the classes "messages highlight"** (actually, those CSS Classes comes with Drupal by default, I just customized them) **and the text would look highlighted according to the design of the theme.**

{% include image src="/files/css%20classes%20example.jpg" alt="CSS classes on Drupal example" description="There are some Drupal modules which lets the user change the content and the layout visually, even the CSS classes, this lets the user change/set the style of the whole content." %}

This would save time and money as well as relieve the IT professional of performing some tasks. Let us see which of all those modules support this capability and how they let it use it.

## Views

> The following paragraphs will only refer to Views 3

This module is one of the most used Drupal modules. It has a lot of capabilities. This also includes CSS classes handling along with HTML container options that let you change the appearance of the output. Views can let you set CSS classes in the container of the View itself, each row and field. Using this feature you can format the whole result of the query.

There are some things that you can do. First, **you can set CSS classes to the View container to change the appearance globally.** This is pretty basic but useful too. Second, **you can set CSS classes to each row/field (you can use it along with the  View container's CSS classes)**. This is used for complex structures that needs multiple identified elements. It is sometimes used along with Views theming (a template "theme hook" suggestion implementation for Views) to implement external libraries and website widgets.

{% include image src="/files/Views-css-class.jpeg" alt="View container's CSS class" description="You can add a CSS class to the whole View, whether to set or to trigger a style." %}

The CSS class field for the View container is pretty simple to see. It is located in the advance menu of the View. However, in order to set a CSS class to the rows. It is necessary that you set the "unformatted list" style to the rows and then set the row container with a custom CSS class. Almost the same happens with the fields. It is necessary that you set the "field" style to the fields and set the container with a custom CSS class.

{% include image src="/files/views-field-css-class.jpg" alt="Views field's CSS classes" description="Views fields and rows can have custom CSS classes too." %}

## Panels

> The following paragraphs will only refer to Panels 3

I should not even say this module is widely used (you should already know it). Panels has the capability to let the user create layouts visually. **The user can set the width, the title, the content (obvious) and CSS properties of each pane** (a pane is an arrangeable representation of a node in the panel). This includes CSS classes. **You can also add CSS properties to each region and the panel container itself** as long as you know the CSS classes. This let create complex layout through the UI without even know how to code.

{% include image src="/files/Panels%20general%20css%20class.jpeg" alt="Panels general settings - CSS classes" %}

{% include image src="/files/panels%20pane%20node%20css%20class.jpeg" alt="Panels pane's CSS classes" %}

Setting CSS classes to the panes is pretty simple. You just have to go to the "CSS properties" option in its menu and add the classes needed. Setting CSS classes to the panel container is also simple. In the general section you can find the "CSS classes" field to do this. However, unlike the "panes" and the panel container, the regions (the container of the panes) are not that simple to customize. In order to set the classes to the regions in Panels (module) you must use a flexible layout customized with the layout builder. The rest of the layouts can not be customized (at least this way). Doing this you can set any CSS class to any region.

## CKeditor

> The following paragraphs will only refer to CKeditor 3

This is a pretty good web text editor. It has a lot of capabilities. **The most interesting about it is that it can create customized DIVs and show the blocks (html elements) visually, letting you edit the properties for the created DIVs. With this you can customize the text in any way you want.** You can create a custom block with a specific CSS class giving a special look to a piece of the text.

{% include image src="/files/ckeditor%20div%20container%20css%20classes.jpg" alt="CKeditor - DIV Container's CSS classes" description="Even any piece of text in CKeditor can have CSS classes." %}

The procedure to create a block was described before already, but first make sure the field supports full HTML inputs. Then hit the "show blocks" button. Select the content for the new block (just hold the mouse button and select the text). Hit the "create DIV container" button and fill the "Stylesheet Classes" field with the CSS Classes needed.

## Context

> The following paragraphs will only refer to Context 3 - Beta 6

This is a great module. **It can let you change** the blocks, menus, other modules behavior and **the properties of the webpage being accessed. It can only let change the properties of the webpage in general**. Not the property of any block or menu, but this is still pretty useful. Perhaps you can trigger a specific style or change the background of the webpage. This is something that may be used efficiently.

{% include image src="/files/context%20css%20class.jpeg" alt="Context - Theme HTML's CSS classes" description="You can set a reaction (with context) that changes the website by using CSS classes." %}

Using CSS classes with Context is pretty simple. Just set some conditions (path, language, node type, ...), make sure whether you want all the conditions to be true or not. Add the "Theme HTML" reaction and set the necessary CSS classes. Although this is a pretty simple change you can set a style which changes when it is inside an element with an specific CSS class. With this you can even change the appearance of the whole webpage.

## Omega

> The following paragraphs will only refer to Omega 3

In my humble opinion, it is the best base theme available for Drupal. Omega can change the layout of the website visually. **Its layout is divided into sections, zones and regions that you can arrange as you need. You can even change the properties of every region and zone, including (of course) the CSS classes.** With other modules such as Delta and Context. You can change the layout of different webpages depending on the conditions (previously set by the user).

{% include image src="/files/omega%20zones%20css%20classes.jpg" alt="Omega zones' CSS Classes" description="The Omega zones can have CSS Classes." %}

{% include image src="/files/omega%20regions%20css%20classes.jpg" alt="Omega regions' CSS classes" description="The Omega regions can also have CSS classes."%}

The CSS classes can be set in the "Zone and region configuration" section in the settings of the Omega subtheme. Select the region or zone to change and change its properties as needed. Delta can save this configuration in order to load it in other occasion. There are some other modules that can use these data. Context is one of them, you can specify a reaction (with Delta) that changes the webpage if some conditions are met.

{% include image src="/files/context%20-%20delta.jpeg" alt="Context - using Delta saved data as a Context Reaction." description="You can use Delta saved data as a Context Reaction." %}

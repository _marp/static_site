---
slug: c-programming-language-obsolete
title: Is C programming language obsolete?
summary: Is it true that everything becomes obsolete by time?, not everything, here we are going to discuss why some old technologies like C programming language are still used these days.
date: 2012-10-09
imgpath: /files/C.png
imgwidth: 480
imgheight: 300
tags: [c-and-c-plus-plus, programming-language, programming]
---
> This was a Draft and remain like it until the blog was abandoned

Is it true that all tools are obsolete by the time?. Is it true that time is the only factor that determine when a programming language is obsolete?, and specially, is C programming language obsolete?. There are many programming languages today and many of them become obsolete over time. Despite that, C programming language has 40 old approximately. There are a lot of programming languages that are widely used on every area, but that should not be a reason to think that the C programming language is obsolete. Let’s define obsolescence as following:

> “Obsolescence is a process where a product, service or practice is considered unwanted, unused, unnecessary or unsupported and it’s being referred as Obsolete or deprecated. A product, service or practice may become Obsolete for different reasons; because of the availability of a replacement that works even better, because it’s simply no longer used or because it’s not supported by any vendor.”.

According to this definition, there are some reasons to think that a technology is obsolete:

- there is a technology, possibly with a lower cost, that works even better than the obsolete technology. Improving the way the things are done on every single task that the obsolete technology was doing, so, a better technology would simply deprecate that old technology.
- The technology is about to be obsolete, because there is no one providing some required materials or supplies like replacements or some required services as technical support or inductions, and there could be another technology that does it. That kind of obsolescence is more used for machinery.
- Because the task that the technology was performing is no longer needed, so the technology is no longer needed as well.

Resuming, we can say that a technology is obsolete when it is no longer needed or it is not helping. Is that the case of C programming language?. Here are the reasons why C isn’t obsolete:

## It’s really useful because of its low level features

The C programming language is still widely used because of its features. It provides a powerful memory management that let programmers explicitly access the memory and set values manually. It also has a thin abstraction layer, so that means that the code is close to the machine code and the program will run faster.

Some high-level programming languages like Java are intended to ease programmer with tools and libraries that make coding simpler, but this leads to a higher resource consumption program. High-level programming languages are being improved now days to make programs lighter and faster. Despite this, high-level programming languages aren’t as suited for projects where performance is a top priority as low-level programming languages like C are.

## It’s most often used for system programming

> “C is often used for ‘system programming’. including implementing operating systems and embedded system applications, due to a combination of desirable characteristics such as code portability and efficiency, ability to access specific hardware addresses, ability to pun types to match externally imposed data access requirements, and low run-time demand on system resources”, Wikipedia.

C is widely used for system programming. C is portable, this means that it’s a small programming language. There is no need to add some extra libraries to run a program. C has better readability than assembly even still providing the control over hardware that assembly has. This makes it ideal for operating systems and some other system related programs.

## It’s most used in embedded systems

It is important to keep in mind that a program should always make a good use of the resources, but, better manufacture processes has made available newer and affordable technology that has made this theory less important. Allowing them to use high-level programming languages to ease programmer, but, there are still some systems with low resources available that are not enough to use such a heavier tool. Using heavier tools like these on embedded systems that just have a few kb of memory. Is not a wise decision.

C is widely used in embedded systems and other systems where resources are limited because of its portability. It’s also used because of its control over hardware and its performance. There are some special features like fixed-point arithmetic, named address spaces, and basic I/O hardware addressing that were added in order to be implemented in embedded systems. This is a non-standard implementation called embedded C.

Besides all these reasons, here are some other reasons why the C programming language shouldn’t be obsolete:

## It’s the most used programming languages on UNIX

> “C is the lingua franca of UNIX development; most people know how to code in it.”, GTK+ / Gnome Application Development. Copyright © 1999 by New Riders Publishing. This material may be distributed only subject to the terms and conditions set forth in the Open Publication License, v1.0 or later (the latest version is presently available at http://www.opencontent.org/openpub/) (this portion was got from the FAQ “Why is GTK+ written in C?”).

The most of the programs created in the most of the UNIX-like distributions are written in C. There’s also some standards like POSIX that assure the compatibility of the programs between operating systems that supports widely C.

## It’s open and standardized

C is standardized. It’s supported by ANSI and ISO. There are also non-standardized implementations like embedded C that are widely supported. There are a lot of compilers available for it. It is available for all major microprocessors and it does not depend on any vendor. It can not be controlled by any organization. These are reasons to consider C a trusted technology and to use it on suitable projects.

## There are a lot of important projects that are still using C

Not just the most of the UNIX-like distributions are written in C. As mentioned before, C is best suitable for projects where performance is the top priority. Projects like; Apache, MySQL, PostgreSQL and Python, are just a few examples of all the programs written in C that are widely used currently.

## Other considerations about C

Although C is still far from obsolete there are some other considerations to be concerned about:

### High-level programming languages are being improved constantly

High-level programming languages like Java or C# are being improved constantly to make programs run faster. The performance difference between low-level and high-level programming languages aren’t great enough to make every project in a low-level programming language like C because these other technologies have more features that makes coding simpler improving the time of development of the program and reducing cost. Most of the high-level programming languages have frameworks that helps programmers to think more about what they do instead of how to code it.

The most of the high-level programming languages deal with memory automatically. Tools like the garbage collector ease programmers to handle a lot of problems related to it. These problems can take considerable time to be repaired. High-level programming languages also provide complex data structures that let programmers make larger programs easily. Helping them to think just about the architecture of the program and not about the data structure itself.

### C has a higher development time and is harder to debug

Using C may require some complex data structures like “link list”. This will make the programmer to build methods for each action to be taken such as add, remove and search. Those methods have to handle the memory directly in order to set the values of the data structure. Besides, the programmer has to think what would be the best way to build the data structure according to the needs of the system. All these requirements can be easily skipped by using a high-level programming language. Saving a lot of time and effort. Additionally, sometimes using these data structures would generate errors like “memory leaks” that can consume a lot of time in order to be fixed. The most of the high-level programming language provide the programmer with flexible data structures and some other tools that allow the programmer work without being worried about all these problems.

### The best way to use C in these times

C programming language is not obsolete yet, but not giving a proper use to it would lead to a waste of time. There are some few reasons to use C. The most common reason is when the program is closely related to hardware or to the system itself especially when resources must be used wisely such as embedded systems, operating systems and so on. C programming language is also used to build parts of other platforms.

C is not obsolete and there are valid reasons to think it is like that. Sometimes the advertisement of new tools such as C# make people think that technologies like C or even C++ are obsolete already. Something that is not true. I personally encourage not asking about this in a forum, not only because people will treat you like you are being an idiot, also because that statement has not a solid base.
